// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];


  class Book {
    constructor(obj){
        this.obj = obj;
    };

    isValidBook() {
        if (!Object.keys(this.obj).includes("author")) {
            throw new InvalidProperty('author');
          }
          if (!Object.keys(this.obj).includes("name")) {
            throw new InvalidProperty('name');
          }
          if (!Object.keys(this.obj).includes("price")) {
            throw new InvalidProperty('price');
          }
    }

    createElement() {
        const root = document.querySelector('#root');
        const rootItem = document.createElement('ul');
        const rootLi = document.createElement('li');
        root.append(rootItem);
        rootItem.append(rootLi)
        rootLi.innerHTML = `${this.obj.author}: ${this.obj.name}`
    }
    render() {
        this.createElement()
        this.isValidBook()
    }
  }


  class InvalidProperty extends Error {
    constructor(book) {
        super();
        this.name = 'Missing';
        this.message = `Invalid property: ${book}`
    }
}

  books.forEach((e) => {
    try {
        new Book(e).render();
    } catch (err) {
        if (err.name === 'Missing') {
            console.log(err)
        } 
        else {
            throw err;
        }
    }
  })
